resource "aws_ecs_task_definition" "lms_backend_dev" {
    family                   = "lms-backend-dev"
    network_mode             = "bridge"
    memory                   = 512
    cpu                      = 256
    container_definitions    = file("${path.module}/container-definitions/lms-backend-dev.json")
    task_role_arn            = "${aws_iam_role.lms_backend_ecs_task_dev.arn}"
    execution_role_arn       = "${aws_iam_role.lms_backend_ecs_exec.arn}"
}

resource "aws_ecs_service" "lms_backend_dev" {
    name            = "lms-backend-dev"
    cluster         = "${aws_ecs_cluster.eduway.id}"
    //task_definition = "${aws_ecs_task_definition.lms_backend_dev.arn}"
    task_definition = "arn:aws:ecs:us-east-1:609962252921:task-definition/lms-backend-dev:39"
    desired_count   = 1
    launch_type     = "EC2"

    load_balancer {
        target_group_arn = "${aws_lb_target_group.graphql_dev.arn}"
        container_port = 4000
        container_name = "lms-backend"
    }
}
