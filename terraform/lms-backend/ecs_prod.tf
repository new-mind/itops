resource "aws_ecs_task_definition" "lms_backend_prod" {
    family                   = "lms-backend-prod"
    network_mode             = "bridge"
    memory                   = 512
    cpu                      = 256
    container_definitions    = file("${path.module}/container-definitions/lms-backend-prod.json")
    task_role_arn            = "${aws_iam_role.lms_backend_ecs_task_prod.arn}"
    execution_role_arn       = "${aws_iam_role.lms_backend_ecs_exec.arn}"
}

resource "aws_ecs_service" "lms_backend_prod" {
    name            = "lms-backend-prod"
    cluster         = "${aws_ecs_cluster.eduway.id}"
    //task_definition = "${aws_ecs_task_definition.lms_backend_prod.arn}"
    task_definition = "arn:aws:ecs:us-east-1:609962252921:task-definition/lms-backend-prod:16"
    desired_count   = 2
    launch_type     = "EC2"

    load_balancer {
        target_group_arn = "${aws_lb_target_group.graphql_prod.arn}"
        container_port = 4000
        container_name = "lms-backend"
    }
}
