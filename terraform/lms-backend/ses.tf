resource "aws_ses_domain_identity" "eduway" {
    domain = "eduway.today"
}

resource "aws_ses_email_identity" "support" {
    email = "support@eduway.today"
}

resource "aws_ses_email_identity" "no-reply" {
    email = "no-reply@eduway.today"
}

resource "aws_ses_template" "student_invitation_ru" {
    name = "student-invitation-ru"
    subject = "Вам добавлен курс"
    html = file("${path.module}/ses_templates/student-invitation-ru.html")
    #text = file("${path.module}/ses_templates/student-invitation-ru.txt")
}

resource "aws_ses_template" "MyTemplate" {
  name    = "MyTemplate"
  subject = "Greetings, {{name}}!"
  html    = "<h1>Hello {{name}},</h1><p>Your favorite animal is {{favoriteanimal}}.</p>"
  text    = "Hello {{name}},\r\nYour favorite animal is {{favoriteanimal}}."
}
