resource "aws_cognito_user_pool" "eduway_prod" {
    name = "eduway-lms-prod"
    alias_attributes = [ "email" ]
    auto_verified_attributes = [ "email" ]
    admin_create_user_config {
        allow_admin_create_user_only = true
    }
    email_configuration {
        email_sending_account = "DEVELOPER"
        source_arn = "${aws_ses_email_identity.support.arn}"
    }
    password_policy {
        minimum_length    = 6
        require_lowercase = false
        require_numbers   = false
        require_symbols   = false
        require_uppercase = false
        temporary_password_validity_days = 180
    }
}

resource "aws_cognito_user_pool_client" "eduway_prod_web" {
    name = "web-app"
    user_pool_id = "${aws_cognito_user_pool.eduway_prod.id}"
    generate_secret = false
}

resource "aws_cognito_identity_pool" "eduway_prod" {
    identity_pool_name = "eduway lms prod"
    allow_unauthenticated_identities = true

    cognito_identity_providers {
        client_id = "${aws_cognito_user_pool_client.eduway_prod_web.id}"
        provider_name = "${aws_cognito_user_pool.eduway_prod.endpoint}"
        server_side_token_check = true
    }
}

resource "aws_cognito_identity_pool_roles_attachment" "eduway_prod" {
    identity_pool_id = "${aws_cognito_identity_pool.eduway_prod.id}"
    roles = {
        "authenticated": "${aws_iam_role.eduway_prod_auth_cognito_identity.arn}",
        "unauthenticated": "${aws_iam_role.eduway_prod_unauth_cognito_identity.arn}"
    }
}

resource "aws_iam_role" "eduway_prod_auth_cognito_identity" {
    name = "eduway-lms-prod-20190610004831-authRole"
    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Federated": "cognito-identity.amazonaws.com"
            },
            "Action": "sts:AssumeRoleWithWebIdentity",
			"Condition": {
				"ForAnyValue:StringLike": {
					"cognito-identity.amazonaws.com:amr": "authenticated"
				}
			}
        }
    ]
}
EOF
}

resource "aws_iam_role" "eduway_prod_unauth_cognito_identity" {
    name = "eduway-lms-prod-20190610004831-unauthRole"
    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Federated": "cognito-identity.amazonaws.com"
            },
            "Action": "sts:AssumeRoleWithWebIdentity",
			"Condition": {
				"ForAnyValue:StringLike": {
					"cognito-identity.amazonaws.com:amr": "unauthenticated"
				}
			}
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "eduway_lms_db_prod" {
    name = "eduway-lms-db-prod"
    role = "${aws_iam_role.eduway_prod_auth_cognito_identity.name}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "dynamodb:*",
            "Resource": [
                "${aws_dynamodb_table.eduway_prod.arn}",
                "${aws_dynamodb_table.eduway_prod.arn}/*",
                "${aws_dynamodb_table.eduway_users_prod.arn}",
                "${aws_dynamodb_table.eduway_users_prod.arn}/*"
            ]
        }
    ]
}
EOF
}
