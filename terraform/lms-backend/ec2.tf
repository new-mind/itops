resource "aws_spot_fleet_request" "lms_backend" {
    iam_fleet_role                      = "arn:aws:iam::609962252921:role/aws-ec2-spot-fleet-tagging-role"
    spot_price                          = 0.07
    allocation_strategy                 = "lowestPrice"
    target_capacity                     = 1
    valid_until                         = "2021-05-01T12:00:00Z"
	terminate_instances_with_expiration = true
	fleet_type							= "maintain"

    launch_specification {
        instance_type               = "m3.medium"
		availability_zone			= "us-east-1a"
		associate_public_ip_address = false
        ami                         = "${var.ecs_ami}"
        spot_price                  = 0.07
        iam_instance_profile_arn    = "arn:aws:iam::609962252921:instance-profile/ecsInstanceRole"
        key_name                    = "drupal"
        user_data                   = file("${path.module}/userdata.txt")
		vpc_security_group_ids      = [ "${aws_security_group.ecs_ec2.id}" ]
		subnet_id                   = "${aws_subnet.public_a.id}"

        root_block_device {
            volume_size = 30
            volume_type = "gp2"
        }

        tags = {
            Name = "eduway-lms-backend"
        }
    }
}

resource "aws_security_group" "ecs_ec2" {
    name   = "eduway-lms-backend-ecs-ec2"
    vpc_id = "${var.vpc_id}"

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }
    ingress {
        from_port       = 0
        to_port         = 65535
        protocol        = "tcp"
		self            = true
        security_groups = [ "${aws_security_group.lb.id}" ]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = [ "0.0.0.0/0" ]
    }
}
