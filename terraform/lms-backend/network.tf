resource "aws_subnet" "public_a" {
    vpc_id                  = "${var.vpc_id}"
    availability_zone       = "us-east-1a"
    cidr_block              = "172.31.0.0/25"
	map_public_ip_on_launch = true

    tags = {
        Name = "eduway-public-a"
    }
}

resource "aws_subnet" "public_b" {
    vpc_id                  = "${var.vpc_id}"
    availability_zone       = "us-east-1b"
    cidr_block              = "172.31.0.128/25"
	map_public_ip_on_launch = true

    tags = {
        Name = "eduway-public-b"
    }
}
