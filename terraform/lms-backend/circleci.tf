resource "aws_iam_user" "circleci" {
    name = "circleci"
}

resource "aws_iam_user_policy" "circleci" {
    name = "circleci"
    user = "${aws_iam_user.circleci.name}"

    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ecr:*"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "ecs:CreateTaskSet",
                "ecs:DescribeServices",
                "ecs:DescribeTaskDefinition",
                "ecs:DeleteTaskSet",
                "ecs:RegisterTaskDefinition",
                "ecs:UpdateService",
                "ecs:UpdateServicePrimaryTaskSet"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "elasticloadbalancing:DescribeTargetGroups",
                "elasticloadbalancing:DescribeListeners",
                "elasticloadbalancing:ModifyListener",
                "elasticloadbalancing:DescribeRules",
                "elasticloadbalancing:ModifyRule"
            ],
            "Resource": [
                "${aws_lb.graphql.arn}",
                "${aws_lb.graphql.arn}/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "iam:PassRole"
            ],
            "Resource": [
                "${aws_iam_role.lms_backend_ecs_task_dev.arn}",
                "${aws_iam_role.lms_backend_ecs_task_prod.arn}",
                "${aws_iam_role.lms_backend_ecs_exec.arn}"
            ]
        }
    ]
}
EOF
}
