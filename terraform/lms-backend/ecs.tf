resource "aws_ecs_cluster" "eduway" {
    name = "eduway"
}

resource "aws_security_group" "ecs_service" {
    name   = "eduway-lms-backend-ecs-service"
    vpc_id = "${var.vpc_id}"

    ingress {
        from_port   = 4000
        to_port     = 4000
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = ["0.0.0.0/0"]
    }
}
