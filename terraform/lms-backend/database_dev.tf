resource "aws_dynamodb_table" "eduway_dev" {
    name         = "eduway-lms-dev"
    billing_mode = "PAY_PER_REQUEST"
    hash_key     = "organizationId"
    range_key    = "SK"

    attribute {
        name = "organizationId"
        type = "S"
    }

    attribute {
        name = "SK"
        type = "S"
    }

    attribute {
        name = "type"
        type = "S"
    }

    attribute {
        name = "courseId"
        type = "S"
    }

    attribute {
        name = "lessonId"
        type = "S"
    }

    attribute {
        name = "userId"
        type = "S"
    }

    global_secondary_index {
        name            = "ByType"
        hash_key        = "organizationId"
        range_key       = "type"
        projection_type = "ALL"
        read_capacity   = 0
        write_capacity  = 0
    }

    global_secondary_index {
        name            = "ByCourse"
        hash_key        = "courseId"
        range_key       = "SK"
        projection_type = "ALL"
        read_capacity   = 0
        write_capacity  = 0
    }

    global_secondary_index {
        name            = "ByLesson"
        hash_key        = "lessonId"
        range_key       = "SK"
        projection_type = "ALL"
        read_capacity   = 0
        write_capacity  = 0
    }

    global_secondary_index {
        name            = "ByUser"
        hash_key        = "userId"
        range_key       = "SK"
        projection_type = "ALL"
        read_capacity   = 0
        write_capacity  = 0
    }

    global_secondary_index {
        name            = "BySK"
        hash_key        = "SK"
        projection_type = "ALL"
        read_capacity   = 0
        write_capacity  = 0
    }
}

resource "aws_dynamodb_table" "eduway_users_dev" {
    name         = "eduway-users-dev"
    billing_mode = "PAY_PER_REQUEST"
    hash_key     = "id"

    attribute {
        name = "id"
        type = "S"
    }

    attribute {
        name = "email"
        type = "S"
    }

    global_secondary_index {
        name            = "ByEmail"
        hash_key        = "email"
        projection_type = "ALL"
    }
}
