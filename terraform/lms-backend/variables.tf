variable vpc_id {
    default = "vpc-6cd14316"
}

// https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html
variable ecs_ami {
    default = "ami-0fac5486e4cff37f4"
}
