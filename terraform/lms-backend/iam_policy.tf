resource "aws_iam_role_policy" "codebuild" {
    name = "eduway-lms-backend-codebuild"
    role = "${aws_iam_role.codebuild.name}"

    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:GetBucketAcl",
        "s3:GetBucketLocation",
        "s3:GetObjectVersion"
      ],
      "Resource": [
        "${aws_s3_bucket.deployment.arn}",
        "${aws_s3_bucket.deployment.arn}/*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "codedeploy" {
    name = "eduway-lms-backend-codedeploy"
    role = "${aws_iam_role.codedeploy.name}"

    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:GetBucketAcl",
        "s3:GetBucketLocation",
        "s3:GetObjectVersion"
      ],
      "Resource": [
        "${aws_s3_bucket.deployment.arn}",
        "${aws_s3_bucket.deployment.arn}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "ecs:*"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "lms_backend_ecs_task_dev" {
    name = "eduway-lms-backend-ecs-task-dev"
    role = "${aws_iam_role.lms_backend_ecs_task_dev.name}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "dynamodb:*"
            ],
            "Resource": [
                "${aws_dynamodb_table.eduway_dev.arn}",
                "${aws_dynamodb_table.eduway_dev.arn}/*",
                "${aws_dynamodb_table.eduway_users_dev.arn}",
                "${aws_dynamodb_table.eduway_users_dev.arn}/*"
            ]
        }, {
            "Effect": "Allow",
            "Action": [
                "cognito-identity:*",
                "cognito-idp:*"
            ],
            "Resource": [
                "arn:aws:cognito-idp:us-east-1:609962252921:userpool/us-east-1_LJLAJrft6",
                "${aws_cognito_user_pool.eduway_dev.arn}"
            ]
        }, {
            "Effect": "Allow",
            "Action": [
                "ses:*"
            ],
            "Resource": [
                "${aws_ses_email_identity.no-reply.arn}",
                "${aws_ses_email_identity.no-reply.arn}/*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "lms_backend_ecs_task_prod" {
    name = "eduway-lms-backend-ecs-task-prod"
    role = "${aws_iam_role.lms_backend_ecs_task_prod.name}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "dynamodb:*"
            ],
            "Resource": [
                "${aws_dynamodb_table.eduway_prod.arn}",
                "${aws_dynamodb_table.eduway_prod.arn}/*",
                "${aws_dynamodb_table.eduway_users_prod.arn}",
                "${aws_dynamodb_table.eduway_users_prod.arn}/*"
            ]
        }, {
            "Effect": "Allow",
            "Action": [
                "cognito-identity:*",
                "cognito-idp:*"
            ],
            "Resource": [
                "${aws_cognito_user_pool.eduway_prod.arn}"
            ]
        }, {
            "Effect": "Allow",
            "Action": [
                "ses:*"
            ],
            "Resource": [
                "${aws_ses_email_identity.no-reply.arn}",
                "${aws_ses_email_identity.no-reply.arn}/*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "lms_backend_ecs_exec" {
    name = "eduway-lms-backend-ecs-exec"
    role = "${aws_iam_role.lms_backend_ecs_exec.name}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}
