resource "aws_lb_target_group" "graphql_dev" {
    name        = "eduway-graphql-dev"
    port        = 80
    protocol    = "HTTP"
    target_type = "instance"
    vpc_id      = "${var.vpc_id}"
    slow_start  = 60

    health_check {
        path    = "/.well-known/apollo/server-health"
        matcher = "200"
    }
}

resource "aws_lb_listener_rule" "graphql_dev" {
    listener_arn = "${aws_lb_listener.graphql.arn}"
    priority     = 100

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.graphql_dev.arn}"
    }

    condition {
        field  = "path-pattern"
        values = ["/dev/*"]
    }
}
