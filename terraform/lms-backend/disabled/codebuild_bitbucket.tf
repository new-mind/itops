resource "aws_codebuild_project" "bitbucket" {
    name          = "eduway-lms-backend-bitbucket"
    service_role  = "${aws_iam_role.codebuild_bitbucket.arn}"

    source {
        type            = "BITBUCKET"
        git_clone_depth = 0
        insecure_ssl    = false
        location        = "https://jiojiajiu@bitbucket.org/new-mind/lms-backend.git"
        buildspec       = file("${path.module}/buildspec_bitbucket.yml")
        auth {
            type = "OAUTH"
        }
    }

    artifacts {
        type                   = "S3"
        location               = "${aws_s3_bucket.deployment.id}"
        path                   = "sources"
        packaging              = "ZIP"
        override_artifact_name = false
    }

    cache {
        type  = "LOCAL"
        modes = ["LOCAL_DOCKER_LAYER_CACHE", "LOCAL_SOURCE_CACHE"]
    }

    environment {
        type                        = "LINUX_CONTAINER"
        compute_type                = "BUILD_GENERAL1_SMALL"
        image                       = "aws/codebuild/standard:2.0"
        image_pull_credentials_type = "CODEBUILD"
    }
}

resource "aws_codebuild_webhook" "bitbucket" {
    project_name = "${aws_codebuild_project.bitbucket.name}"
}

resource "aws_iam_role" "codebuild_bitbucket" {
    name               = "eduway-lms-backend-codebuild-bitbucket"
    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "codebuild.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "codebuild_bitbucket" {
    name   = "eduway-lms-backend-codebuild-bitbucket"
    role   = "${aws_iam_role.codebuild_bitbucket.name}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:GetBucketAcl",
                "s3:GetBucketLocation",
                "s3:GetObjectVersion"
            ],
            "Resource": [
                "${aws_s3_bucket.deployment.arn}",
                "${aws_s3_bucket.deployment.arn}/*"
            ]
        }
    ]
}
EOF
}
