resource "aws_codebuild_project" "lms_backend" {
    name = "eduway-lms-backend-bitbucket"
    service_role  = "${aws_iam_role.codebuild_bitbucket.arn}"

    artifacts {
        type = "CODEPIPELINE"
    }

    cache {
        type  = "LOCAL"
        modes = ["LOCAL_DOCKER_LAYER_CACHE", "LOCAL_SOURCE_CACHE"]
    }

    environment {
        compute_type                = "BUILD_GENERAL1_SMALL"
        image                       = "aws/codebuild/standard:2.0"
        type                        = "LINUX_CONTAINER"
        image_pull_credentials_type = "CODEBUILD"
    }

    source {
        type            = "CODEPIPELINE"
        buildspec       = file("${path.module}/buildspec.yml")
    }
}
