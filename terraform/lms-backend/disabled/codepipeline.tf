resource "aws_codepipeline" "lms_backend" {
    name     = "eduway-lms-backend"
    role_arn = "${aws_iam_role.codepipeline.arn}"

    artifact_store {
        location = "${aws_s3_bucket.deployment.bucket}"
        type     = "S3"
    }

    stage {
        name = "Source"
        action {
            name             = "Source"
            category         = "Source"
            owner            = "AWS"
            provider         = "S3"
            version          = "1"
            output_artifacts = ["source_output"]
            configuration    = {
                PollForSourceChanges = false
                S3Bucket = "${aws_s3_bucket.deployment.bucket}"
                S3ObjectKey = "sources/eduway-lms-backend-bitbucket"
            }
        }
    }

    stage {
        name = "Build"

        action {
            name             = "Build"
            category         = "Build"
            owner            = "AWS"
            provider         = "CodeBuild"
            input_artifacts  = ["source_output"]
            output_artifacts = ["build_output"]
            version          = "1"

            configuration = {
                ProjectName = "${aws_codebuild_project.lms_backend.name}"
            }
        }
    }

    stage {
        name = "Deploy"
        action {
            name            = "Deploy"
            category        = "Deploy"
            owner           = "AWS"
            provider        = "ECS"
            input_artifacts = ["source_output"]
            version         = "1"
            run_order       = "1"

            configuration   = {
                ClusterName = "${aws_ecs_cluster.eduway.name}"
                ServiceName = "test"
            }
        }
    }
}

resource "aws_iam_role" "codepipeline" {
    name               = "eduway-lms-backend-codepipeline"
    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "codepipeline.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "codepipeline" {
    name   = "eduway-lms-backend-codepipeline"
    role   = "${aws_iam_role.codepipeline.id}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect":"Allow",
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:GetObjectVersion",
                "s3:GetBucketVersioning"
            ],
            "Resource": [
                "${aws_s3_bucket.deployment.arn}",
                "${aws_s3_bucket.deployment.arn}/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "codebuild:BatchGetBuilds",
                "codebuild:StartBuild"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}
