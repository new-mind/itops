resource "aws_cognito_user_pool" "eduway_dev" {
    name = "eduway-lms-dev"
    alias_attributes = [ "email" ]
    auto_verified_attributes = [ "email" ]
    admin_create_user_config {
        allow_admin_create_user_only = true
    }
    email_configuration {
        email_sending_account = "DEVELOPER"
        source_arn = "${aws_ses_email_identity.support.arn}"
    }
    password_policy {
        minimum_length    = 6
        require_lowercase = false
        require_numbers   = false
        require_symbols   = false
        require_uppercase = false
        temporary_password_validity_days = 7
    }
}

resource "aws_cognito_user_pool_client" "eduway_dev_web" {
    name = "web-app"
    user_pool_id = "${aws_cognito_user_pool.eduway_dev.id}"
    generate_secret = false
}

resource "aws_cognito_identity_pool" "eduway_dev" {
    identity_pool_name = "eduway lms dev"
    allow_unauthenticated_identities = true

    cognito_identity_providers {
        client_id = "${aws_cognito_user_pool_client.eduway_dev_web.id}"
        provider_name = "${aws_cognito_user_pool.eduway_dev.endpoint}"
        server_side_token_check = true
    }
}

resource "aws_cognito_identity_pool_roles_attachment" "eduway_dev" {
    identity_pool_id = "${aws_cognito_identity_pool.eduway_dev.id}"
    roles = {
        "authenticated": "${aws_iam_role.auth_cognito_identity.arn}",
        "unauthenticated": "${aws_iam_role.unauth_cognito_identity.arn}"
    }
}

resource "aws_iam_role" "auth_cognito_identity" {
    name = "eduway-lms-dev-20190610004942-authRole"
    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Federated": "cognito-identity.amazonaws.com"
            },
            "Action": "sts:AssumeRoleWithWebIdentity",
			"Condition": {
				"ForAnyValue:StringLike": {
					"cognito-identity.amazonaws.com:amr": "authenticated"
				}
			}
        }
    ]
}
EOF
}

resource "aws_iam_role" "unauth_cognito_identity" {
    name = "eduway-lms-dev-20190610004942-unauthRole"
    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Federated": "cognito-identity.amazonaws.com"
            },
            "Action": "sts:AssumeRoleWithWebIdentity",
			"Condition": {
				"ForAnyValue:StringLike": {
					"cognito-identity.amazonaws.com:amr": "unauthenticated"
				}
			}
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "eduway_lms_db_dev" {
    name = "eduway-lms-db-dev"
    role = "${aws_iam_role.auth_cognito_identity.name}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "dynamodb:*",
            "Resource": [
                "${aws_dynamodb_table.eduway_dev.arn}",
                "${aws_dynamodb_table.eduway_dev.arn}/*",
                "${aws_dynamodb_table.eduway_users_dev.arn}",
                "${aws_dynamodb_table.eduway_users_dev.arn}/*"
            ]
        }
    ]
}
EOF
}
