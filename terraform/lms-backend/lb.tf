resource "aws_lb" "graphql" {
    name               = "eduway-graphql"
    internal           = false
    load_balancer_type = "application"
    security_groups    = [ "${aws_security_group.lb.id}" ]
    subnets            = [ "${aws_subnet.public_a.id}", "${aws_subnet.public_b.id}" ]
}

resource "aws_security_group" "lb" {
    name = "lb"
    vpc_id = "${var.vpc_id}"

    ingress {
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = [ "0.0.0.0/0" ]
    }
}

data "aws_acm_certificate" "eduway" {
    domain = "eduway.today"
}

resource "aws_lb_listener" "graphql" {
    load_balancer_arn = "${aws_lb.graphql.arn}"
    port              = 443
    protocol          = "HTTPS"
    ssl_policy        = "ELBSecurityPolicy-2016-08"
    certificate_arn   = "${data.aws_acm_certificate.eduway.arn}"

    default_action {
        type             = "fixed-response"
        fixed_response {
            content_type = "text/plain"
            message_body = "What you gonna do?"
            status_code  = 200
        }
    }
}

data "aws_route53_zone" "eduway" {
    name = "eduway.today."
}

resource "aws_route53_record" "graphql" {
    zone_id = "${data.aws_route53_zone.eduway.zone_id}"
    name    = "graphql.eduway.today"
    type    = "CNAME"
    ttl     = 500
    records = ["${aws_lb.graphql.dns_name}"]
}
