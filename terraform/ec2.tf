resource "aws_instance" "eduway" {
    ami                         = "ami-0ee745ad390f15056"
    availability_zone           = "us-east-1b"
    ebs_optimized               = false
    instance_type               = "t2.micro"
    monitoring                  = false
    key_name                    = "drupal"
    subnet_id                   = "subnet-c06d2e9c"
    vpc_security_group_ids      = [
        "sg-054f81bb034a82b5e"
    ]
    associate_public_ip_address = true
    private_ip                  = "172.31.41.108"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 10
        delete_on_termination = true
    }

    tags = {
        Name = "eduway1"
    }
}

