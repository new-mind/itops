resource "aws_route53_record" "eduway-today-NS" {
    zone_id = "Z2JF3G339NW9J9"
    name    = "eduway.today"
    type    = "NS"
    records = ["ns-1598.awsdns-07.co.uk.", "ns-626.awsdns-14.net.", "ns-1321.awsdns-37.org.", "ns-206.awsdns-25.com."]
    ttl     = "172800"

}

resource "aws_route53_record" "eduway-today-SOA" {
    zone_id = "Z2JF3G339NW9J9"
    name    = "eduway.today"
    type    = "SOA"
    records = ["ns-1598.awsdns-07.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

resource "aws_route53_record" "mail-eduway-today-CNAME" {
    zone_id = "Z2JF3G339NW9J9"
    name    = "mail.eduway.today"
    type    = "CNAME"
    records = ["domain.mail.yandex.net"]
    ttl     = "3600"

}
