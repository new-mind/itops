resource "aws_iam_group" "admins" {
  name = "admins"
  path = "/"
}

resource "aws_iam_group_membership" "admins" {
  name  = "admins-group-membership"
  users = [
    "guro.bokum",
    "yegor.litvyakov"
  ]
  group = "admins"
}

resource "aws_iam_user" "guro-bokum" {
  name = "guro.bokum"
  path = "/"
}
