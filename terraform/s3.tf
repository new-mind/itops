resource "aws_s3_bucket" "k8s" {
  bucket = "eduway-k8s"

  versioning {
    enabled = true
  }
}
