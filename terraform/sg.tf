resource "aws_security_group" "k8s_control_pane" {
    name   = "k8s_control_pane"
    vpc_id = "vpc-6cd14316"

    # Kubernetes API server
    ingress {
        from_port   = 6443
        to_port     = 6443
        protocol    = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    # etcd server client API
    ingress {
        from_port   = 2379
        to_port     = 2380
        protocol    = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    # Kubelet API
    ingress {
        from_port   = 10250
        to_port     = 10250
        protocol    = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    # kube-sheduler
    ingress {
        from_port   = 10251
        to_port     = 10251
        protocol    = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    # kube-controller-manager
    ingress {
        from_port   = 10252
        to_port     = 10252
        protocol    = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }
}

resource "aws_security_group" "k8s_worker" {
    name = "k8s_worker"
    vpc_id = "vpc-6cd14316"

    # Kubelet API
    ingress {
        from_port   = 10250
        to_port     = 10250
        protocol    = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    # NodePort services
    ingress {
        from_port   = 30000
        to_port     = 32767
        protocol    = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }
}
