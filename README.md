# ITOps

IT Operations

## Terraform

```
$ terraform version
Terraform v0.12.3

$ cd terraform/
$ ./tf init
$ ./tf plan
```

## Kubernetes

```
$ kops version
Version 1.12.2

$ cd k8s/
```
